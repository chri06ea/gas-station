using Microsoft.VisualStudio.TestTools.UnitTesting;
using Gas_Station;

namespace UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            //Arrange
            var gasStation = new GasStation();

            //Act
            gasStation.DepositGas(100.0);

            //Assert
            Assert.AreEqual((int)100.0, (int)gasStation.BuyGas(10000.0));
        }

        [TestMethod]
        public void TestMethod2()
        {
            //Arrange
            var gasStation = new GasStation();

            //Act
            
            //Assert
            Assert.AreEqual((int)1000.0, (int)gasStation.DepositGas(2000.0));
        }
    }
}
