﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gas_Station
{
    public class GasTank
    {
        public GasTank()
        {
        }

        public GasTank(double initialGasAmount, double gasLimit)
        {
            this.gasAmount = initialGasAmount;
            this.gasLimit = gasLimit;
        }

        public double Pump(double amountToPump)
        {
            double amountPumped = amountToPump;

            if (amountToPump > gasAmount)
                amountPumped = gasAmount;

            gasAmount -= amountPumped;

            return amountPumped;
        }

        public double Deposit(double amountToDeposit)
        {
            

            if (amountToDeposit + gasAmount > gasLimit)
                amountToDeposit = gasLimit - gasAmount;

            gasAmount += amountToDeposit;

            return amountToDeposit;
        }

        double gasAmount = 0.0;
        double gasLimit = 100.0;
    }
}
